﻿using System;
using System.Activities;
using System.Activities.Statements;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Exchange.WebServices.Data;


namespace Exchange_Mail_Activities
{
    public class Convert_String_To_SecureString : CodeActivity
    {
        [Category("Input")]
        [DisplayName("Password")]
        [Description("String, it is not recommended to store password as string.  See documentation for more info.")]
        [RequiredArgument]
        public InArgument<String> StrPass { get; set; }

        [Category("Output")]
        [DisplayName("Securestring")]
        [Description("SecureString, it is not recommended to store password as string.  See documentation for more info.")]
        [RequiredArgument]
        public OutArgument<SecureString> Pass { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            //not secure, as password is visible in memory - but if someone stores password as plain text it really doesn't matter at this point
            string pass = StrPass.Get(context);
            if (pass == "")
            {
                throw new Exception("Input string is empty, cannot convert!");
            }
            else
            {
                //generate securestring
                SecureString theSecureString = new NetworkCredential("", pass).SecurePassword;
                //return value
                Pass.Set(context, theSecureString);
            }
        }
    }

    public class Create_Exchange_Connection : CodeActivity
    {
        [Category("Logon")]
        [DisplayName("Username")]
        [Description("Username i.e.: user@domain.com.")]
        [RequiredArgument]
        public InArgument<string> Username { get; set; }

        [Category("Logon")]
        [DisplayName("Password")]
        [Description("Password, secure string.")]
        [RequiredArgument]
        public InArgument<SecureString> Password { get; set; }

        [Category("Connection")]
        [Description("Do you want to use Autodiscover?")]
        [RequiredArgument]
        public InArgument<bool> Autodiscover { get; set; }

        [Category("Connection")]
        [Description("If not using Autodiscover, provide server address.")]
        public InArgument<string> Server { get; set; }

        [Category("Connection")]
        [Description("If not using Autodiscover, provide domain.")]
        public InArgument<string> Domain { get; set; }

        [Category("Output")]
        [DisplayName("Exchange Service")]
        [Description("Returns Exchange service which stores your connection and can be used with other activities.")]
        [RequiredArgument]
        public OutArgument<ExchangeService> ExchangeService { get; set; }

        public Create_Exchange_Connection()
        {
            this.Server = new InArgument<string>("https://outlook.office365.com");
            this.Autodiscover = new InArgument<bool>(true);
        }

        protected override void Execute(CodeActivityContext context)
        {
            bool autodisc = Autodiscover.Get(context);
            string server = Server.Get(context);
            string domain = Domain.Get(context);
            string username = Username.Get(context);
            SecureString pass = Password.Get(context);

            //TODO: prevent having this in memory?
            IntPtr ptr = Marshal.SecureStringToBSTR(pass);
            string s_pass = Marshal.PtrToStringBSTR(ptr);

            ExchangeService exchange = new ExchangeService(ExchangeVersion.Exchange2013_SP1);

            if (autodisc == true)
            {
                try
                {
                    exchange.Credentials = new WebCredentials(username, s_pass, domain);
                    exchange.AutodiscoverUrl(username, RedirectionUrlValidationCallback);
                    //set exchange service for output argument
                    ExchangeService.Set(context, exchange);
                    Console.WriteLine("Create_Exchange_Connection: Autodiscover complete");
                }
                catch (Exception ex)
                {
                    exchange = null;
                    throw new Exception("Autodiscover connection failed:" + ex.ToString());
                }
            }
            else
            {
                if (server != null)
                {
                    try
                    {
                        Uri uri = new Uri(server + "/EWS/Exchange.asmx");
                        exchange.Credentials = new WebCredentials(username, s_pass);
                        exchange.Url = uri;
                        //set exchange service for output argument
                        ExchangeService.Set(context, exchange);
                        Console.WriteLine("Create_Exchange_Connection: Connection without Autodiscover");
                    }
                    catch (Exception ex)
                    {
                        exchange = null;
                        throw new Exception("Connection try failed:" + ex.ToString());

                    }

                }
                else
                {
                    throw new Exception("Please provide server name!");
                }
            }
        }

        internal static bool RedirectionUrlValidationCallback(string redirectionUrl)
        {
            // The default for the validation callback is to reject the URL.
            bool result = false;

            Uri redirectionUri = new Uri(redirectionUrl);

            // Validate the contents of the redirection URL. In this simple validation
            // callback, the redirection URL is considered valid if it is using HTTPS
            // to encrypt the authentication credentials. 
            if (redirectionUri.Scheme == "https")
            {
                result = true;
            }

            return result;
        }
    }
    /*
    public class Create_Exchange_Directories : CodeActivity
    {
        [Category("Input")]
        [DisplayName("Exchange Service")]
        [Description("Existing Exchange service")]
        [RequiredArgument]
        public InArgument<ExchangeService> ExchangeService { get; set; }

        [Category("Input")]
        [DisplayName("Directories")]
        [Description("Single column DataTable of directories to be created")]
        [RequiredArgument]
        public InArgument<DataTable> DirsDT { get; set; }

        [Category("Input")]
        [DisplayName("Processed Folder")]
        [Description("Master folder for processed invoices subfolders")]
        [RequiredArgument]
        public InArgument<String> Processed { get; set; }

        [Category("Input")]
        [DisplayName("Not Processed Folder")]
        [Description("Master folder for NOT processed invoices subfolders")]
        [RequiredArgument]
        public InArgument<String> NotProcessed { get; set; }

        [Category("Input")]
        [DisplayName("Reminders")]
        [Description("Folder for reminders/statements and other documents")]
        [RequiredArgument]
        public InArgument<String> Reminders { get; set; }

        [Category("Input")]
        [DisplayName("Manual Check")]
        [Description("Folder for invoices to be checked manually by human")]
        [RequiredArgument]
        public InArgument<String> ManualCheck { get; set; }

        public Create_Exchange_Directories()
        {
            //define default vars
            this.Processed = new InArgument<string>("Processed");
            this.NotProcessed = new InArgument<string>("Not Processed");
            this.Reminders = new InArgument<string>("Reminders");
            this.ManualCheck = new InArgument<string>("Manual Check");
        }

        protected override void Execute(CodeActivityContext context)
        {
            ExchangeService exchange = ExchangeService.Get(context);
            System.Data.DataTable dirsDT = DirsDT.Get(context);
            string remindersDir = Reminders.Get(context);
            string processedDir = Processed.Get(context);
            string notProcessedDir = NotProcessed.Get(context);
            string manualCheckDir = ManualCheck.Get(context);

            //put data from dt column 0 to list
            var dirsList = dirsDT.AsEnumerable().Select(r => r.Field<string>(0)).ToList();

            if (exchange != null)
            {
                //create master directories
                Folder processed = new Folder(exchange){ DisplayName = processedDir };
                processed.Save(WellKnownFolderName.MsgFolderRoot);
                var pFid = processed.Id;

                Folder notProcessed = new Folder(exchange){ DisplayName = notProcessedDir };    
                notProcessed.Save(WellKnownFolderName.MsgFolderRoot);
                var npFid = notProcessed.Id;

                Folder reminders = new Folder(exchange) { DisplayName = remindersDir };
                reminders.Save(WellKnownFolderName.MsgFolderRoot);
                var rFid = reminders.Id;

                Folder manualcheck = new Folder(exchange) { DisplayName = manualCheckDir };
                manualcheck.Save(WellKnownFolderName.MsgFolderRoot);
                var mcFid = manualcheck.Id;

                //iterate through whole list
                foreach (string dir in dirsList)
                {
                    //processed
                    Folder folder = new Folder(exchange)
                    {
                        DisplayName = dir,
                    };
                    // Create as subdir
                    folder.Save(pFid);

                    //not processed
                    Folder folder2 = new Folder(exchange)
                    {
                        DisplayName = dir,
                    };
                    folder2.Save(npFid);
                }
            }
            else
            {
                throw new Exception("No valid connection found");
            }

        }
    }
    */
    public class Get_Exchange_Messages : CodeActivity
    {
        [Category("Input")]
        [Description("Exchange service")]
        [DisplayName("Existing Exchange Service")]
        [RequiredArgument]
        public InArgument<ExchangeService> ExchangeService { get; set; }

        [Category("Options")]
        [Description("How many e-mails do you want to retrieve?")]
        [RequiredArgument]
        public InArgument<int> Number { get; set; }

        [Category("Options")]
        [DisplayName("Mailfolder")]
        [Description("Mail folder from which messages will be retrieved.")]
        [RequiredArgument]
        public InArgument<string> Dir { get; set; }
  
        [Category("Options")]
        [DisplayName("Mailbox")]
        [Description("Mailbox, usually the same as user. Shared mailbox can also be used.")]
        [RequiredArgument]
        public InArgument<string> Mailbox { get; set; }

        [Category("Output")]
        [Description("Returns list of messages.")]
        [RequiredArgument]
        public OutArgument<List<EmailMessage>> MessagesList { get; set; }

        public Get_Exchange_Messages()
        {
            this.Number = new InArgument<int>(500);
            this.Dir = new InArgument<string>("Inbox");
            //this.ContentType = new InArgument<string>("plain");
        }

        protected override void Execute(CodeActivityContext context)
        {
            int emails = Number.Get(context);
            string dir = Dir.Get(context);
            string mailbox = Mailbox.Get(context);
            ExchangeService exchange = ExchangeService.Get(context);
            //string contentType = ContentType.Get(context);

            if (exchange != null)
            {
                //creates an object that will represent the desired mailbox
                Mailbox mb = new Mailbox(@mailbox);

                FolderId fid = null;

                if (dir == "Inbox")
                {
                    //creates a folder object that will point to inbox folder
                    fid = new FolderId(WellKnownFolderName.Inbox, mb);
                    Console.WriteLine("Get_Exchange_Messages: Reading from \"Inbox\"...");
                }
                else
                {
                    Console.WriteLine("Get_Exchange_Messages: Reading from \"{0}\"", dir);
                    //SetView
                    FolderView view = new FolderView(500);
                    view.PropertySet = new PropertySet(BasePropertySet.IdOnly);
                    view.PropertySet.Add(FolderSchema.DisplayName);
                    //to see all folders!
                    view.Traversal = FolderTraversal.Deep;

                    //have to assign folder id, otherwise if using shared mailbox - we will see main mailbox folders instead sharedmailbox
                    fid = new FolderId(WellKnownFolderName.MsgFolderRoot, mb);

                    /*
                    Folder rootfolder = Folder.Bind(exchange, WellKnownFolderName.MsgFolderRoot);
                    rootfolder.Load();
                    Console.WriteLine("Rootfolder: {0}", rootfolder.DisplayName);
                    */

                    //to get sharedmailbox working for non-wellknownfolders
                    FindFoldersResults findFolderResults = exchange.FindFolders(fid, view);

                    //foreach (Folder folder in rootfolder.FindFolders(view))
                    foreach (Folder folder in findFolderResults)
                    {
                        //destination folder found
                        try
                        {
                            if (folder.DisplayName == dir)
                            {
                                // Stores the Folder ID in a variable
                                fid = folder.Id;
                            }
                        }
                        catch
                        {
                            throw new Exception(string.Format("Could not find folder {0}.", dir));
                        }
                    }
                }

                //creates a folder object that will point to inbox folder

                if (fid != null)
                {
                    //bind the mailbox using service instance
                    Folder inbox = Folder.Bind(exchange, fid);

                    //load items from mailbox folder
                    if (inbox != null)
                    {
                        bool more = true;
                        //ItemView view = new ItemView(int.MaxValue, 0, OffsetBasePoint.Beginning);
                        ItemView view = new ItemView(emails, 0, OffsetBasePoint.Beginning);

                        view.PropertySet = PropertySet.IdOnly;
                        FindItemsResults<Item> findResults;
                        List<EmailMessage> listEmails = new List<EmailMessage>();
                        while (more & listEmails.Count < emails)
                        {
                            //find folder using fid
                            findResults = exchange.FindItems(fid, view);
                            foreach (var item in findResults.Items)
                            {
                                listEmails.Add((EmailMessage)item);
                                //item.Load();
                            }
                            more = findResults.MoreAvailable;
                            if (more)
                            {
                                view.Offset += 1000;
                            }
                        }


                        //check if there are any emails, if list is empty, we cannot load properties
                        if (listEmails.Any())
                        {
                            //load properties, so we can read their values
                            PropertySet itemPropertySet = new PropertySet(BasePropertySet.FirstClassProperties);
                            //add some non-standard properties
                            itemPropertySet.Add(ItemSchema.TextBody);
                            itemPropertySet.Add(ItemSchema.MimeContent);

                            exchange.LoadPropertiesForItems(listEmails, itemPropertySet);
                        }

                        MessagesList.Set(context, listEmails);

                    }
                    else
                    {
                        Console.WriteLine("Get_Exchange_Messages: No valid connection found");
                    }
                }
                else
                {
                    throw new Exception(string.Format("Could not find folder {0}.", dir));
                }
            }
        }
    }

    public class Move_Message : CodeActivity
    {
        [Category("Input")]
        [Description("Exchange service")]
        [DisplayName("Existing Exchange Service")]
        [RequiredArgument]
        public InArgument<ExchangeService> ExchangeService { get; set; }

        [Category("Input")]
        [Description("EWS EmailMessage")]
        [DisplayName("EmailMessage")]
        [RequiredArgument]
        public InArgument<EmailMessage> Mail { get; set; }

        [Category("Input")]
        [Description("Where do you want to move message?")]
        [DisplayName("Move to folder")]
        [RequiredArgument]
        public InArgument<string> MoveTo { get; set; }

        [Category("Options")]
        [DisplayName("SharedMailbox")]
        [Description("Address of Sharedmailbox if you are using one.")]
        public InArgument<string> Mailbox { get; set; }

        [Category("Output")]
        [Description("Indicates if activity has been executed successfully (true) or not (false)")]
        [DisplayName("Success")]
        public OutArgument<bool> Success { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            bool success = true;
            Success.Set(context, success);

            try
            {
                ExchangeService exchange = ExchangeService.Get(context);
                EmailMessage mail = Mail.Get(context);
                string moveTo = MoveTo.Get(context);
                string mailbox = Mailbox.Get(context);
                //exchange limit
                int maxFolders = 500;
                bool folderFound = false;

                Mailbox mb = new Mailbox(@mailbox);

                //SetView
                FolderView view = new FolderView(maxFolders);
                view.PropertySet = new PropertySet(BasePropertySet.IdOnly);
                view.PropertySet.Add(FolderSchema.DisplayName);
                //DeepTraversal, to see all folders!
                view.Traversal = FolderTraversal.Deep;

                //if we are woking with sharedmailbox
                if (mailbox != null)
                {
                    FolderId fid = new FolderId(WellKnownFolderName.MsgFolderRoot, mb);
                    //to get sharedmailbox working for non-wellknownfolders
                    FindFoldersResults findFolderResults = exchange.FindFolders(fid, view);

                    foreach (Folder folder in findFolderResults)
                    {
                        //destination folder found
                        if (folder.DisplayName == moveTo)
                        {
                            // Stores the Folder ID in a variable
                            var folderId = folder.Id;
                            //Console.WriteLine(fid);
                            // Load the email, move it to the specified folder
                            mail.Load();
                            mail.Move(folderId);
                            folderFound = true;
                        }

                    }
                }
                else
                {
                    Folder rootfolder = Folder.Bind(exchange, WellKnownFolderName.MsgFolderRoot);
                    //Folder rootfolder = Folder.Bind(exchange, WellKnownFolderName.Root);
                    rootfolder.Load();

                    //Console.WriteLine("Move_Message: Move to:" + moveTo);

                    foreach (Folder folder in rootfolder.FindFolders(view))
                    {
                        //destination folder found
                        if (folder.DisplayName == moveTo)
                        {
                            // Stores the Folder ID in a variable
                            var fid = folder.Id;
                            //Console.WriteLine(fid);
                            // Load the email, move it to the specified folder
                            mail.Load();
                            mail.Move(fid);
                            folderFound = true;
                        }
                    }
                }


                if (folderFound == false)
                {
                    throw new Exception(string.Format("Could not find folder {0}.", moveTo));
                }
            }
            catch (Exception ex)
            {
                success = false;
                Success.Set(context, success);
                throw new Exception("Error: " + ex.ToString());
            }
        }
    }

    public class Retrieve_Attachment : CodeActivity
    {
        [Category("Input")]
        [Description("Path to .msg or *.eml file")]
        [DisplayName("Path to message file")]
        [RequiredArgument]
        public InArgument<string> Input { get; set; }

        [Category("Input")]
        [Description("Folder path where attachments should be saved")]
        [DisplayName("Target directory path")]
        [RequiredArgument]
        public InArgument<string> SaveTo { get; set; }

        [Category("Optional")]
        [Description("If file with the same name already exist in directory, rename a new file?")]
        [DisplayName("Rename if exists")]
        public InArgument<bool> Rename { get; set; }

        [Category("Optional")]
        [Description("Generate random name when saving?")]
        [DisplayName("Always rename files")]
        public InArgument<bool> RenameAll { get; set; }

        [Category("Optional")]
        [Description("Do you want to see activity logs in Output window?")]
        [DisplayName("Activity logs visible")]
        public InArgument<bool> Logs { get; set; }

        [Category("Optional")]
        [Description("Do you want to filter out items with ContentID assigned? - these should be embedded/inline images, not real attachments")]
        [DisplayName("ContentID filter")]
        public InArgument<bool> Filter { get; set; }

        [Category("Output")]
        [Description("Indicates if activity has been executed successfully (true) or not (false)")]
        [DisplayName("Success")]
        public OutArgument<bool> Success { get; set; }

        public Retrieve_Attachment()
        {
            //define default vars
            this.Rename = new InArgument<bool>(false);
            this.RenameAll = new InArgument<bool>(false);
            this.Logs = new InArgument<bool>(false);
            this.Filter = new InArgument<bool>(false);
        }

        protected override void Execute(CodeActivityContext context)
        {
            bool success = true;
            try
            {
                //get values from activity properties
                string input = Input.Get(context);
                string saveTo = SaveTo.Get(context);
                bool rename = Rename.Get(context);
                bool alwaysRename = RenameAll.Get(context);
                bool logsVisible = Logs.Get(context);
                bool useContentId = Filter.Get(context);

                //provided file doesn't exist, throw exception
                if (!File.Exists(input))
                {
                    throw new Exception("File doesn't exists: " + input);
                }

                //.eml
                if (Path.GetFileName(input).EndsWith(".eml") || Path.GetFileName(input).EndsWith(".EML")) //TODO: tolower?
                {
                    //get file info
                    var fileInfo = new FileInfo(@input);
                    //create message
                    var eml = MsgReader.Mime.Message.Load(fileInfo);

                    if (eml.Attachments.Count > 0)
                    {
                        foreach (MsgReader.Mime.MessagePart attach in eml.Attachments)
                        {
                            if (useContentId)
                            {
                                if (attach.ContentId == null)
                                {
                                    SaveAttachedEML(attach, alwaysRename, rename, saveTo, logsVisible);
                                }
                                else
                                {
                                    if (logsVisible == true) { Console.WriteLine("Retrieve_Attachment: {0}, is embedded/inline image, not real attachment.", attach.FileName); }
                                }
                            }
                            else
                            {
                                SaveAttachedEML(attach, alwaysRename, rename, saveTo, logsVisible);
                            }
                        }
                    }
                    else
                    {
                        string subject = "e-mail";
                        if (eml.Headers.Subject != null) { subject = eml.Headers.Subject; }
                        if (logsVisible == true) { Console.WriteLine("Retrieve_Attachment: nothing attached to {0}", subject); }
                    }
                }
                //.msg
                else if (Path.GetFileName(input).EndsWith(".msg") || Path.GetFileName(input).EndsWith(".MSG"))
                {
                    //create new Outlook message from file
                    var msg = new MsgReader.Outlook.Storage.Message(input);
                    msg.Save(msg.Subject.Replace(":", ""));

                    if (msg.Attachments.Count > 0)
                    {
                        foreach (MsgReader.Outlook.Storage.Attachment attach in msg.Attachments)
                        {
                            if (useContentId)
                            {
                                if (attach.ContentId == null)
                                {
                                    SaveAttachedMSG(attach, alwaysRename, rename, saveTo, logsVisible);
                                }
                                else
                                {
                                    if (logsVisible == true) { Console.WriteLine("Retrieve_Attachment: {0}, is embedded/inline image, not real attachment.", attach.FileName); }
                                }
                            }
                            else
                            {
                                SaveAttachedMSG(attach, alwaysRename, rename, saveTo, logsVisible);
                            }
                        }
                    }
                    else
                    {
                        string subject = "e-mail";
                        if (msg.Subject != null) { subject = msg.Subject; }
                        if (logsVisible == true) { Console.WriteLine("Retrieve_Attachment: nothing attached to {0}", subject); }
                    }
                }
                else
                {
                    //not supported file, throw exception
                    throw new Exception("This is not correct .msg/.eml file!");
                }
            }
            catch (Exception ex)
            {
                success = false;
                throw new Exception("Error: " + ex.ToString());
            }

            Success.Set(context, success);
        }

        public void SaveAttachedEML(MsgReader.Mime.MessagePart attach, bool alwaysRename, bool rename, string saveTo, bool logsVisible)
        {
            //remove fobidden characters from original file name
            string saveName = Regex.Replace(attach.FileName, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
            //generate random name for file
            if (alwaysRename == true)
            {
                //remove random extension, bring back original one
                saveName = Path.GetRandomFileName().Replace(".", string.Empty) + Path.GetExtension(saveName);
            }

            //rename new file if already exists, so old one will not be overwritten
            if (rename == true)
            {
                if (File.Exists(@saveTo + saveName))
                {
                    saveName = Path.GetRandomFileName().Replace(".", string.Empty) + Path.GetExtension(saveName);
                }
            }

            byte[] attachBytes = attach.Body;
            FileStream attachStream = File.Create(@saveTo + saveName);
            attachStream.Write(attachBytes, 0, attachBytes.Length);
            attachStream.Close();
            if (logsVisible == true) { Console.WriteLine("Retrieve_Attachment: file {0}, saved as {1}", attach.FileName, saveName); }
        }

        public void SaveAttachedMSG(MsgReader.Outlook.Storage.Attachment attach, bool alwaysRename, bool rename, string saveTo, bool logsVisible)
        {
            //generate random name for file
            string saveName = attach.FileName;
            if (alwaysRename == true)
            {
                saveName = Path.GetRandomFileName().Replace(".", string.Empty) + Path.GetExtension(saveName);
            }

            //rename new file if already exists, so old one will not be overwritten
            if (rename == true)
            {
                if (File.Exists(@saveTo + saveName))
                {
                    saveName = Path.GetRandomFileName().Replace(".", string.Empty) + Path.GetExtension(saveName);
                }
            }

            //rename if exist - or not, give option
            byte[] attachBytes = attach.Data;
            FileStream attachStream = File.Create(@saveTo + saveName);
            attachStream.Write(attachBytes, 0, attachBytes.Length);
            attachStream.Close();
            if (logsVisible == true) { Console.WriteLine("Retrieve_Attachment: file {0}, saved as {1}", attach.FileName, saveName); }
        }
    }

    public class Send_Message : CodeActivity
    {
        [Category("Connection")]
        [Description("Exchange service")]
        [DisplayName("Existing Exchange Service")]
        [RequiredArgument]
        public InArgument<ExchangeService> ExchangeService { get; set; }

        [Category("Message")]
        [Description("E-mail address of sender")]
        [DisplayName("1. Sender")]
        [RequiredArgument]
        public InArgument<string> Sender { get; set; }

        [Category("Message")]
        [Description("Subject of message")]
        [DisplayName("2. Subject")]
        [RequiredArgument]
        public InArgument<string> Sub { get; set; }

        [Category("Message")]
        [Description("Body of message")]
        [DisplayName("3. Body")]
        [RequiredArgument]
        public InArgument<string> Bod { get; set; }

        [Category("Message")]
        [Description("Recipient(s) of message")]
        [DisplayName("4. Recipient")]
        [RequiredArgument]
        public InArgument<string> To { get; set; }

        [Category("Message")]
        [Description("Send copy to")]
        [DisplayName("5. CC")]
        public InArgument<string> CC { get; set; }

        [Category("Message")]
        [Description("Send hidden copy to")]
        [DisplayName("6. BCC")]
        public InArgument<string> BCC { get; set; }

        [Category("Options")]
        [Description("By default copy is saved in Sent folder. But you can save it whereever you want.")]
        [DisplayName("Save copy in")]
        public InArgument<string> Dir { get; set; }

        [Category("Options")]
        [Description("Is body HTML(true) or plain text(false)?")]
        [DisplayName("HTML Body")]
        [RequiredArgument]
        public InArgument<bool> HTML { get; set; }

        [Category("Output")]
        [Description("Indicates if activity has been executed successfully (true) or not (false)")]
        [DisplayName("Success")]
        public OutArgument<bool> Success { get; set; }

        public Send_Message()
        {
            this.Dir = new InArgument<string>("default");
            this.HTML = new InArgument<bool>(false);
        }

        protected override void Execute(CodeActivityContext context)
        {
            bool success = true;
            Success.Set(context, success);

            try
            {
                ExchangeService exchange = ExchangeService.Get(context);
                string subject = Sub.Get(context);
                string body = Bod.Get(context);
                string recipient = To.Get(context);
                string directory = Dir.Get(context);
                string sender = Sender.Get(context);
                string cc = CC.Get(context);
                string bcc = BCC.Get(context);
                bool isHtml = HTML.Get(context);

                //create an email message using exisitng exchange service
                EmailMessage message = new EmailMessage(exchange)
                {
                    //add properties
                    Subject = subject,
                    Sender = sender,
                    Body = body,
                };

                //send this to
                message.ToRecipients.Add(recipient);
                message.CcRecipients.Add(cc);
                message.BccRecipients.Add(bcc);

                if (isHtml) { message.Body.BodyType = BodyType.HTML; }
                else { message.Body.BodyType = BodyType.Text; }

                if (directory == "default") { message.SendAndSaveCopy(); }
                else
                {
                    //SetView, set to 500 as you canywah cant have more folders using exchange
                    FolderView view = new FolderView(500);
                    view.PropertySet = new PropertySet(BasePropertySet.IdOnly);
                    view.PropertySet.Add(FolderSchema.DisplayName);
                    //to see all folders!
                    view.Traversal = FolderTraversal.Deep;

                    Folder rootfolder = Folder.Bind(exchange, WellKnownFolderName.MsgFolderRoot);
                    rootfolder.Load();

                    bool folderFound = false;

                    foreach (Folder folder in rootfolder.FindFolders(view))
                    {
                        //destination folder found
                        if (folder.DisplayName == directory)
                        {
                            // Stores the Folder ID in a variable
                            var fid = folder.Id;
                            //Console.WriteLine(fid);
                            // Load the email, move it to the specified folder
                            folderFound = true;
                            //save copy in found folder
                            message.SendAndSaveCopy(fid);
                        }
                    }

                    if (folderFound == false)
                    {
                        throw new Exception(string.Format("Could not find folder {0}.", directory));
                    }
                }
            }
            catch (Exception ex)
            {
                success = false;
                Success.Set(context, success);
                throw new Exception("Error: " + ex.ToString());
            }
        }
    }

    public class Save_Attachments : CodeActivity
    {
        [Category("Input")]
        [Description("EWS EmailMessage")]
        [DisplayName("EmailMessage")]
        [RequiredArgument]
        public InArgument<EmailMessage> Mail { get; set; }

        [Category("Input")]
        [Description("Folder path where attachments should be saved")]
        [DisplayName("Target directory path")]
        [RequiredArgument]
        public InArgument<string> SaveTo { get; set; }

        [Category("Optional")]
        [Description("Extensions you want to save, separate extensions by | i.e: 'pdf|PDF' for .pdf files.")]
        [DisplayName("Filter")]
        public InArgument<string> Extensions { get; set; }

        [Category("Optional")]
        [Description("If file with the same name already exist in directory, rename new file?")]
        [DisplayName("Rename if exists")]
        public InArgument<bool> Rename { get; set; }

        [Category("Optional")]
        [Description("Generate random name when saving?")]
        [DisplayName("Always rename files")]
        public InArgument<bool> RenameAll { get; set; }

        [Category("Output")]
        [Description("Indicates if activity has been executed successfully (true) or not (false)")]
        [DisplayName("Success")]
        public OutArgument<bool> Success { get; set; }

        public Save_Attachments()
        {
            //define default vars
            this.Rename = new InArgument<bool>(false);
            this.RenameAll = new InArgument<bool>(false);
        }

        protected override void Execute(CodeActivityContext context)
        {
            bool success = true;
            Success.Set(context, success);

            //ExchangeService exchange = ExchangeService.Get(context);
            EmailMessage msg = Mail.Get(context);
            msg.Load();

            string path = SaveTo.Get(context);
            string pattern = Extensions.Get(context);
            bool rename = Rename.Get(context);
            bool alwaysRename = RenameAll.Get(context);

            if (msg.HasAttachments)
            {
                try
                {
                    foreach (Attachment attach in msg.Attachments)
                    {
                        //there is not filter, save all
                        if (pattern == null)
                        {
                            SaveFile(attach, path, rename, alwaysRename);
                        }
                        else
                        {
                            string[] arrPatterns = pattern.Split('|');
                            //iterate through filters
                            foreach (string p in arrPatterns)
                            {
                                if (attach.Name.EndsWith(p)) { SaveFile(attach, path, rename, alwaysRename); }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    success = false;
                    Success.Set(context, success);
                    throw new Exception("Error: " + ex.ToString());
                }
            }
            else { Console.WriteLine("Save_Attachments: nothing attached."); }
        }

        public void SaveFile(Attachment attach, string path, bool rename, bool alwaysRename)
        {
            if (attach is FileAttachment) //file
            {
                FileAttachment fileAttachment = attach as FileAttachment;
                fileAttachment.Load();
                string saveName = fileAttachment.Name; //as filename not exists yet

                //generate random name for file
                if (alwaysRename == true)
                {
                    saveName = Path.GetRandomFileName().Replace(".", string.Empty) + Path.GetExtension(fileAttachment.Name);
                }

                if (rename == true)
                {
                    if (File.Exists(path + fileAttachment.Name))
                    {
                        saveName = Path.GetRandomFileName().Replace(".", string.Empty) + Path.GetExtension(fileAttachment.Name);
                    }
                }
                //regex to remove forbidden characters - yes, it happens!
                fileAttachment.Load(@path + Regex.Replace(saveName, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled));
            }
            else if (attach is ItemAttachment) //email's
            {
                ItemAttachment itemAttachment = attach as ItemAttachment;
                itemAttachment.Load(EmailMessageSchema.MimeContent);

                string fileExtension = "";
                string saveName = itemAttachment.Name;
                Console.WriteLine(saveName);

                //by default ItemAttachment has not extension, so have to handle it
                if (itemAttachment.Item is EmailMessage)
                {
                    fileExtension = ".eml";
                }
                else if (itemAttachment.Item is Contact)
                {
                    fileExtension = ".vcf";
                }
                else if (itemAttachment.Item is Appointment)
                {
                    fileExtension = ".ics";
                }
                else //just in case, if null or return empty string and accidentally there will be some extension!
                {
                    fileExtension = Path.GetExtension(itemAttachment.Name);
                }

                //generate random name for file
                if (alwaysRename == true)
                {
                    saveName = Path.GetRandomFileName().Replace(".", string.Empty);
                }

                //rename new file if already exists, so old one will not be overwritten
                if (rename == true)
                {
                    if (File.Exists(path + itemAttachment.Name + fileExtension))
                    {
                        saveName = Path.GetRandomFileName().Replace(".", string.Empty);
                    }
                }

                File.WriteAllBytes(@path + Regex.Replace(saveName, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled) + fileExtension, itemAttachment.Item.MimeContent.Content);
            }
        }
    }
}
