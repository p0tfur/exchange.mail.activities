## Exchange Mail Activities
is a MIT licensed package of several activities to manage mailbox using Microsoft Exchange Webservices (EWS). At this moment this is only fully working set of activites based on EWS for UiPath:

*  handle up to 500 folders (500 is max for Exchange server),
*  read as many messages from server as you want (there is no limit to 1000 messages!) also from Sharedmailbox,
*  save all file attachments, filtering by extension if needed, and removing forbidden chars from file names, so file is saved instead of throwing exception (i.e. a lot of invoicing systems use forbidden characters for invoices files names),

### Contains activites:

*  Convert String To SecureString, it is not recommended to store your password as string, but if you have to this activity will convert it to SecureString.
*  Create Exchange Connection, allows you to create a connection to Exchange Server which can be used later with other activites. You can connect using Autodiscover or providing domain and server address.
*  Get Exchange Messages, retrieves messages from server and returns as List<EmailMessage>. Can pull as many messages as you want (not limited to 1000), handles also SharedMailBox.
*  Move Message, move e-mail message from current location to provided mailfolder.
*  Save Attachments, save all attachments from provided <EmailMessage>. Attachments can be filtered by extensions, optionally files can be renamed if file with the same name exists or can be always renamed. If original names should be keeped, forbidden chars will be removed from name if there are some.
*  Retrieve Attachment, retrieve attachments from messages (.eml or .msg) saved on drive and saves them in provided location. Additional filtering/renaming options like above available.
*  Send e-mail, send plain text or HTML message using Exchange.

